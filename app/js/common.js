$(function () {

	// Owl-Carousel
	$('.owl-home').owlCarousel({
		loop: true,
		nav: false,
		dotsContainer: '#customDots',
		items: 1,
		autoplay: true,
		autoplayTimeout: 5000
	})

	// Load the Visualization API and the corechart package.
	google.charts.load("current", { packages: ['corechart'] });
	google.charts.setOnLoadCallback(drawChart);
	function drawChart() {
		var data = google.visualization.arrayToDataTable([
			["Element", "Проценты", { role: "style" }, { role: "annotation" }],
			["от 18 до 21", 21, "#FF906A", '21%'],
			["от 22 до 29", 23, "#FE7B4F", '23%'],
			["от 30 до 50", 56, "#FB5F2A", '56%']
		]);

		var view = new google.visualization.DataView(data);
		view.setColumns([0, 1, 2,
			{
				calc: "stringify",
				sourceColumn: 3,
				type: "string",
				role: "annotation"
			},
		]);

		var options = {
			fontSize: 16,
			annotations: {
				textStyle: {
					fontSize: 16,
					bold: true,
					color: '#181818',
					auraColor: '#181818',
					opacity: 1
				}
			},		
			backgroundColor: "transparent",
			title: "",
			vAxis: {
				title: '%',
				titleTextStyle: {
					color: '#636363',
					italic: false,
					fontSize: 16
				}
			},
			hAxis: {
				title: 'Лет',
				titleTextStyle: {
					color: '#636363',
					italic: false,
					fontSize: 16
				}
			},
			height: 350,
			bar: { groupWidth: "47" },
			legend: { position: "none" },
		};
		var chart = new google.visualization.ColumnChart(document.getElementById("column_chart"));
		chart.draw(view, options);
	}
	//donut_chart
	google.charts.load('current', { 'packages': ['corechart'] });
	google.charts.setOnLoadCallback(drawChart2);

	function drawChart2() {

		var data = google.visualization.arrayToDataTable([
			['gender', 'Percent'],
			['Женщины', 44],
			['Мужчины', 56],
		]);

		var options = {
			backgroundColor: "transparent",
			pieHole: 0.65,
			legend: { position: 'bottom', textStyle: { fontSize: 18 }, alignment: 'center' },
			slices: {
				0: { color: '#5AA6FF' },
				1: { color: '#DD3800' }
			},
			chartArea: { left: 0, top: 10, width: '100%', height: '325' }
		};

		var chart = new google.visualization.PieChart(document.getElementById('donut_chart'));
		chart.draw(data, options);
	}
	//Pie Chart
	google.charts.load('current', { 'packages': ['corechart'] });
	google.charts.setOnLoadCallback(drawChart3);

	function drawChart3() {

		var data = google.visualization.arrayToDataTable([
			['gender', 'Percent'],
			['Взрослое население', 50],
			['Руководители и специалисты', 37],
			['Служащие', 13],
		]);

		var options = {
			backgroundColor: "transparent",
			legend: { position: 'right', textStyle: { fontSize: 18 }, alignment: 'center' },
			slices: {
				0: { color: '#FF6C3A' },
				1: { color: '#FBC02A' },
				2: { color: '#5AA6FF' }
			},
			chartArea: { left: 0, top: 10, width: '80%', height: '248' }
		};

		var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));
		chart.draw(data, options);
	}
	//svg icon
	jQuery('img.svg').each(function () {
		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		jQuery.get(imgURL, function (data) {
			// Get the SVG tag, ignore the rest
			var $svg = jQuery(data).find('svg');

			// Add replaced image's ID to the new SVG
			if (typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if (typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass + ' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');

			// Check if the viewport is set, else we gonna set it if we can.
			if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
				$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
			}

			// Replace image with new SVG
			$img.replaceWith($svg);

		}, 'xml');

	});
	//CALC SLIDER
	function calculation(quantity, time) {
		var price = 240;
		var k = 0.5;
		var result = 0;

		if ( quantity == 270 ) {
			price = 140;
		} else if ( quantity < 270 && quantity >= 180 ) {
			price = 160;
		} else if ( quantity < 180 && quantity >= 84 ) {
			price = 190;
		} else if ( quantity < 84 && quantity >= 42 ) {
			price = 215;
		}

		if ( time >= 60 ) {
			k = 2;
		} else if ( time >= 26 && time < 60 ) {
			k = 1.2;
		} else if ( time >=20 && time <= 25 ) {
			k = 1;
		} else if ( time >=16 && time <= 19 ) {
			k = 0.8;
		}

		result = quantity * price * k;

		// 1 трансляция
		$('#price_one_broadcasting').text(price*k + " руб.");
		$('#form-price_one_broadcasting').val(price*k + " руб.");

		// Итого
		$('#price_result').text(result + " руб.");
		$('#form-price_result').val(result + " руб.");

	}

	$("#slider-range-broadcasting").slider({
		range: "min",
		value: 10,
		min: 1,
		max: 270,
		slide: function (event, ui) {
			$("#amount-broadcasting").val(ui.value + " шт.");
			$("#form-amount-broadcasting").val(ui.value + " шт.");
			$("#dup-amount-broadcasting").text(ui.value);
			calculation( ui.value, $("#slider-range-time").slider("value") );
		}
	});
	$("#amount-broadcasting").val($("#slider-range-broadcasting").slider("value") + " шт.");
	$("#form-amount-broadcasting").val($("#slider-range-broadcasting").slider("value") + " шт.");
	$("#dup-amount-broadcasting").text($("#slider-range-broadcasting").slider("value"));
	
	$("#slider-range-time").slider({
		range: "min",
		value: 30,
		min: 5,
		max: 100,
		slide: function (event, ui) {
			$("#amount-time").val(ui.value + " сек");
			$("#form-amount-time").val(ui.value + " сек");
			$("#dup-amount-time").text(ui.value);
			calculation( $("#slider-range-broadcasting").slider("value"), ui.value );
		}
	});
	calculation(10,30);
	$("#amount-time").val($("#slider-range-time").slider("value") + " сек");
	$("#form-amount-time").val($("#slider-range-time").slider("value") + " сек");
	$("#dup-amount-time").text($("#slider-range-time").slider("value"));

	//Плавный скролл
	$('.scroll-to').on('click', function (event) {
		event.preventDefault();
		var sc = $(this).attr("href"),
			dn = $(sc).offset().top;
		$('html, body').animate({ scrollTop: dn }, 1000);
	});

	//E-mail Ajax Send
	$("form").submit(function () { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "/wp-content/themes/shanson/mail.php", //Change
			data: th.serialize()
		}).done(function () {
			th.prev("h2").text("Заявка отправлена!");
			th.prev("h2").addClass("green");
			th.toggle();
			th.after("<p class='result'>Наш менеджер перезвонит<br> Вам в течение 10 минут</p>");
			setTimeout(function () {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});

	//MaskedInput
	$('input[type="tel"]').mask("+7-999-999-99-99");
});
